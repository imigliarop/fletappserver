package App;

import java.util.HashMap;
import java.util.Map;
import javax.ws.rs.ext.ContextResolver;
import org.glassfish.jersey.moxy.json.MoxyJsonConfig;
import org.glassfish.jersey.server.ResourceConfig;
import org.json.simple.JSONObject;

@javax.ws.rs.ApplicationPath("api")
public class ApplicationConfig extends ResourceConfig {

    public ApplicationConfig(){
        createApp();
    }
    
    public static ResourceConfig createApp() {
        return new ResourceConfig()
                .packages("org.glassfish.jersey.examples.jsonmoxy")
                .register(createMoxyJsonResolver())
                .register(CorsFilter.class)
                .register(JSONObject.class);
    }

    public static ContextResolver<MoxyJsonConfig> createMoxyJsonResolver() {
        final MoxyJsonConfig moxyJsonConfig = new MoxyJsonConfig();
        Map<String, String> namespacePrefixMapper = new HashMap<>();
        namespacePrefixMapper.put("http://www.w3.org/2001/XMLSchema-instance", "xsi");
        
        moxyJsonConfig
                .setNamespacePrefixMapper(namespacePrefixMapper)
                .setNamespaceSeparator(':');
        return moxyJsonConfig.resolver();
    }

}
