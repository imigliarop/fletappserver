/**
 * This file was generated by the Jeddict
 */
package Entities;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 * @author amcro
 */
@Entity
public class Fletero extends Usuario {

    @ManyToOne(targetEntity = Vehiculo.class)
    private Vehiculo vehiculo;

    @OneToMany(targetEntity = ZonaDeTrabajo.class)
    private List<ZonaDeTrabajo> zonasDeTrabajo;

    @OneToMany(targetEntity = Flete.class, mappedBy = "fletero")
    private List<Flete> fletes;

    public Vehiculo getVehiculo() {
        return this.vehiculo;
    }

    public void setVehiculo(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }

    public List<ZonaDeTrabajo> getZonasDeTrabajo() {
        return this.zonasDeTrabajo;
    }

    public void setZonasDeTrabajo(List<ZonaDeTrabajo> zonasDeTrabajo) {
        this.zonasDeTrabajo = zonasDeTrabajo;
    }

    public List<Flete> getFletes() {
        return this.fletes;
    }

    public void setFletes(List<Flete> fletes) {
        this.fletes = fletes;
    }

}
