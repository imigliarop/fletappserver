package BusinessLogic;

import DTO.DTOUsuario;
import DataAccess.UsuarioManager;
import DataAccess.UsuarioClienteManager;
import DataAccess.UsuarioFleteroManager;
import DataAccess.VehiculoManager;
import DataAccess.ZonaDeTrabajoManager;
import Entities.Cliente;
import Entities.Fletero;
import Entities.Usuario;
import Entities.Vehiculo;
import Entities.ZonaDeTrabajo;
import Enums.TipoUsuario;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class BlUsuario {

    UsuarioManager usuarioManager;
    UsuarioFleteroManager usuarioFleteroManager;
    ZonaDeTrabajoManager zonaDeTrabajoManager;
    VehiculoManager vehiculoManager;
    EntityManager entityManager;

    public BlUsuario() {
        entityManager = Persistence.createEntityManagerFactory("FletappPU").createEntityManager();
    }

    public List<ZonaDeTrabajo> AgregarZonaDeTrabajo(Long fleteroId, ZonaDeTrabajo zonaDeTrabajo) {
        usuarioFleteroManager = new UsuarioFleteroManager(entityManager);
        zonaDeTrabajoManager = new ZonaDeTrabajoManager(entityManager);

        //Creo la zona de trabajo
        zonaDeTrabajoManager.grabarZonaDeTrabajo(zonaDeTrabajo);

        //Obtengo el fletero y le agrego la zona de trabajo
        Fletero f = usuarioFleteroManager.obtenerUsuario(fleteroId);
        f.getZonasDeTrabajo().add(zonaDeTrabajo);

        //Actualizo el fletero
        usuarioFleteroManager.actualizarUsuario(f);

        return f.getZonasDeTrabajo();
    }

    public String ObtenerFleterosParaSugerir(List<String> zonasIds, Double volumen, Double peso, Double distancia) {
        usuarioFleteroManager = new UsuarioFleteroManager(entityManager);
        if (volumen == null) {
            volumen = (double) 0;
        }
        if (peso == null) {
            peso = (double) 0;
        }

        List<Fletero> fleteros = usuarioFleteroManager.obtenerFleterosParaSugerir(zonasIds, volumen, peso);
        JSONArray respuesta = new JSONArray();

        for (int i = 0; i < fleteros.size(); i++) {
            List<ZonaDeTrabajo> zonas = fleteros.get(i).getZonasDeTrabajo();
            List<ZonaDeTrabajo> zonasDelPedido = new ArrayList<>();

            //Recorro las zonas del fletero y elijo las que piden
            for (int j = 0; j < zonas.size(); j++) {
                if (zonasIds.contains(zonas.get(j).getId())) {
                    zonasDelPedido.add(zonas.get(j));
                }
            }

            //Comparo los precios y me quedo con el mas alto
            Double precioAMostrar = zonasDelPedido.get(0).getPrecio();
            for (int j = 1; j < zonasDelPedido.size(); j++) {
                if (precioAMostrar < zonasDelPedido.get(j).getPrecio()) {
                    precioAMostrar = zonasDelPedido.get(j).getPrecio();
                }
            }

            JSONObject json = new JSONObject();
            json.put("nombre", fleteros.get(i).getNombre() + " " + fleteros.get(i).getApellido());
            json.put("vehiculo", fleteros.get(i).getVehiculo().getNombre());
            json.put("costo", Math.round((precioAMostrar * distancia) * 100.0) / 100.0);
            respuesta.add(json);
        }

        return respuesta.toJSONString();
    }

    public String ObtenerZonasDeFletero(Long fleteroId) {
        usuarioFleteroManager = new UsuarioFleteroManager(entityManager);
        Fletero fletero = usuarioFleteroManager.obtenerUsuario(fleteroId);
        JSONArray respuesta = new JSONArray();

        List<ZonaDeTrabajo> zonas = fletero.getZonasDeTrabajo();

        for (ZonaDeTrabajo zona : zonas) {
            JSONObject json = new JSONObject();
            json.put("id", zona.getId());
            json.put("nombre", zona.getNombre());
            json.put("precio", zona.getPrecio());
            respuesta.add(json);
        }
        return respuesta.toJSONString();
    }

    public Vehiculo ObtenerVehiculoDeFletero(Long fleteroId) {
        usuarioFleteroManager = new UsuarioFleteroManager(entityManager);
        Fletero fletero = usuarioFleteroManager.obtenerUsuario(fleteroId);
        return fletero.getVehiculo();
    }

    public Vehiculo GuardarVehiculo(Long fleteroId, Vehiculo vehiculo) {
        usuarioFleteroManager = new UsuarioFleteroManager(entityManager);
        vehiculoManager = new VehiculoManager(entityManager);

        //Grabar o actualizar el vehiculo
        if (vehiculo.getId() == null || vehiculo.getId() == 0) {
            vehiculoManager.grabarVehiculo(vehiculo);
        } else {
            vehiculoManager.actualizarVehiculo(vehiculo);
        }
        
        //Actualizar el fletero
        Fletero fletero = usuarioFleteroManager.obtenerUsuario(fleteroId);
        fletero.setVehiculo(vehiculo);
        usuarioFleteroManager.actualizarUsuario(fletero);
        
        return vehiculo;
    }
}
