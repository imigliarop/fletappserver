package BusinessLogic;

import DTO.DTOUsuario;
import DataAccess.UsuarioClienteManager;
import DataAccess.UsuarioFleteroManager;
import DataAccess.UsuarioManager;
import Entities.Cliente;
import Entities.Fletero;
import Entities.Usuario;
import Enums.TipoUsuario;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

public class BLAuth {
    UsuarioManager usuarioManager;
    EntityManager entityManager;
    
    public BLAuth(){
        entityManager = Persistence.createEntityManagerFactory("FletappPU").createEntityManager();
    }
    
    public void CrearUsuario(DTOUsuario dtoUsuario) throws Exception{
        Usuario usu;
        
        switch (dtoUsuario.getTipoUsuario()){
            case Cliente:
                usu = new Cliente();
                usuarioManager = new UsuarioClienteManager(entityManager);
                break;
            case Fletero:
                usu = new Fletero();
                usuarioManager = new UsuarioFleteroManager(entityManager);
                break;
            default:
                throw new Exception("Tipo de usuario incorrecto");
        }
        
        if (usuarioManager.existeEmail(dtoUsuario.getEmail())) {
            throw new Exception("Ya existe un usuario con el email ingresado");
        }
        
        usu.setNombre(dtoUsuario.getNombre());
        usu.setApellido(dtoUsuario.getApellido());
        usu.setPassword(dtoUsuario.getPassword());
        
        List<String> emails = new ArrayList<>();
        emails.add(dtoUsuario.getEmail());
        usu.setEmails(emails);
        
        List<String> telefonos = new ArrayList<>();
        telefonos.add(dtoUsuario.getTelefono());
        usu.setTelefonos(telefonos);
        
        usuarioManager.grabarUsuario(usu);
    }
    
    public Usuario Autenticar(String email, String password, TipoUsuario tipo) throws Exception
    {
        switch (tipo) {
            case Cliente:
                usuarioManager = new UsuarioClienteManager(entityManager);
                break;
            case Fletero:
                usuarioManager = new UsuarioFleteroManager(entityManager);
                break;
            default:
                throw new Exception();
        }
        
        if(!usuarioManager.existeEmail(email)) {
            throw new Exception("No existe un usuario con ese email");
        }
        
        Usuario usu = usuarioManager.obtenerUsuario(email);
        
        if (usu.getPassword() == null ? password != null : !usu.getPassword().equals(password)) {
            throw new Exception("La contraseña es incorrecta");
        }
        
        return usu;
    }
}
