package BusinessLogic;

import DTO.DTODetalleFlete;
import DTO.DTOFiltroFechas;
import DTO.DTOPosibles;
import DTO.DTOPrecioFlete;
import DTO.DTOSolicitudFlete;
import DataAccess.FleteManager;
import DataAccess.UsuarioClienteManager;
import DataAccess.UsuarioFleteroManager;
import Entities.Cliente;
import Entities.Flete;
import Entities.Fletero;
import Entities.ZonaDeTrabajo;
import Enums.EstadoFlete;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

public class BlFlete {

    FleteManager fleteManager;
    UsuarioClienteManager clienteManager;
    UsuarioFleteroManager fleteroManager;
    EntityManager entityManager;

    public BlFlete() {
        entityManager = Persistence.createEntityManagerFactory("FletappPU").createEntityManager();
    }

    public Long SolicitarFlete(DTOSolicitudFlete solicitud) {
        Flete flete = new Flete();

        //Para probar, si la fecha es null que le cargue la fecha de hoy
        if (solicitud.getFechaHora() == null) {
            solicitud.setFechaHora(new Date(Calendar.getInstance().getTimeInMillis()));
        }

        flete.setFechaHora(solicitud.getFechaHora());
        flete.setPeso(solicitud.getPeso());
        flete.setVolumen(solicitud.getVolumen());
        flete.setPrecioSolicitado(solicitud.getPrecioSolicitado());
        flete.setEstado(EstadoFlete.Solicitado);
        flete.setPuntoOrigen(solicitud.getPuntoOrigen());
        flete.setPuntoDestino(solicitud.getPuntoDestino());

        //Obtener el cliente que creo el flete
        clienteManager = new UsuarioClienteManager(entityManager);
        Cliente cliente = clienteManager.obtenerUsuario(solicitud.getClienteId());
        flete.setCliente(cliente);

        fleteManager = new FleteManager(entityManager);
        fleteManager.grabarFlete(flete);

        return flete.getId();
    }

    public List<DTODetalleFlete> ObtenerProximosFletes(Long fleteroId) {
        fleteroManager = new UsuarioFleteroManager(entityManager);
        clienteManager = new UsuarioClienteManager(entityManager);

        List<DTODetalleFlete> fletes = new ArrayList<>();
        Fletero fletero = fleteroManager.obtenerUsuario(fleteroId);

        for (Flete flete : fletero.getFletes()) {
            if (flete.getEstado() != EstadoFlete.Confirmado) {
                continue;
            }
            DTODetalleFlete detalle = new DTODetalleFlete((long) 0, flete.getCliente().getNombre(), flete.getCliente().getApellido(),
                    flete.getFechaHora(), flete.getPrecioFinal(), flete.getPrecioSolicitado(), flete.getPeso(), flete.getVolumen(), EstadoFlete.Solicitado, 0,
                    flete.getPuntoOrigen(), flete.getPuntoDestino());

            fletes.add(detalle);
        }

        return fletes;
    }

    public List<DTODetalleFlete> obtenerListaFletesPendientesCliente(DTOFiltroFechas filtro) {
        clienteManager = new UsuarioClienteManager(entityManager);

        List<DTODetalleFlete> fletes = new ArrayList<>();
        List<Flete> listFletes = clienteManager.listarFletesPendientesCliente(filtro.getUsuarioId(), filtro.getFechaDesde(), filtro.getFechaHasta());

        for (int i = 0; i < listFletes.size(); i++) {

            DTODetalleFlete detalle = new DTODetalleFlete(listFletes.get(i).getId(), null, null,
                    listFletes.get(i).getFechaHora(), listFletes.get(i).getPrecioFinal(), listFletes.get(i).getPrecioSolicitado(), listFletes.get(i).getPeso(), listFletes.get(i).getVolumen(), listFletes.get(i).getEstado(), listFletes.get(i).getPuntuacion(),
                    listFletes.get(i).getPuntoOrigen(), listFletes.get(i).getPuntoDestino());

            fletes.add(detalle);
        }

        return fletes;
    }

    public List<DTODetalleFlete> obtenerListaFletesRealizadosCliente(DTOFiltroFechas filtro) {
        clienteManager = new UsuarioClienteManager(entityManager);

        List<DTODetalleFlete> fletes = new ArrayList<>();
        List<Flete> listFletes = clienteManager.listarFletesRealizadosCliente(filtro.getUsuarioId(), filtro.getFechaDesde(), filtro.getFechaHasta());

        for (int i = 0; i < listFletes.size(); i++) {

            DTODetalleFlete detalle = new DTODetalleFlete(listFletes.get(i).getId(), listFletes.get(i).getFletero().getNombre(), listFletes.get(i).getFletero().getApellido(),
                    listFletes.get(i).getFechaHora(), listFletes.get(i).getPrecioFinal(), listFletes.get(i).getPrecioSolicitado(), listFletes.get(i).getPeso(), listFletes.get(i).getVolumen(), listFletes.get(i).getEstado(), listFletes.get(i).getPuntuacion(),
                    listFletes.get(i).getPuntoOrigen(), listFletes.get(i).getPuntoDestino());

            fletes.add(detalle);
        }

        return fletes;
    }

    public List<DTODetalleFlete> obtenerFletesProximoPosible(Long idFletero,Date horaFinFlete, List<DTOPosibles> fletesPosibles) {
        fleteroManager = new UsuarioFleteroManager(entityManager);
        Fletero fletero = fleteroManager.obtenerUsuario(idFletero);
        List<ZonaDeTrabajo> zonas = fletero.getZonasDeTrabajo();
        List<DTOPrecioFlete> fletePrecio = new ArrayList<>();
        // RESUMO LA LISTA DE FLETES CON ZONAS QUEDANDOME SOLO CON LAS DEL FLETERO
        for (DTOPosibles posible : fletesPosibles) {
            for (ZonaDeTrabajo zona : zonas) {
                String zonastring = "zonas." + posible.getZona().toString();
                if (zona.getId().equals(zonastring)) {
                    fletePrecio.add(new DTOPrecioFlete("fletes." + posible.getPtoInicio().toString(), zona.getPrecio()));
                }
            }
        }
        //QUEDARME SOLO CON LOS FLETES Y SUS PRECIO MENOR
        List<DTOPrecioFlete> fletePrecioAux = fletePrecio;
        List<DTOPrecioFlete> fletePrecioResult = new ArrayList<>();
        for (DTOPrecioFlete fletePosible : fletePrecio) {
            Boolean esMenor = true;
            for (DTOPrecioFlete fletePosibleAux : fletePrecioAux) {
                if (fletePosible.getFleteId().equals(fletePosibleAux.getFleteId())) {
                    if (fletePosible.getPrecio() > fletePosibleAux.getPrecio()) {
                        esMenor = false;
                    }
                }
            }
            if (esMenor) {
                fletePrecioResult.add(fletePosible);
            }
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(horaFinFlete);
        cal.add(Calendar.MINUTE, 45);
        Date horaAComparar = cal.getTime();
        fleteManager = new FleteManager(entityManager);
        List<Flete> listFletes = new ArrayList<>();
        Double volumen = fletero.getVehiculo().getVolumenMaximo();
        Double peso = fletero.getVehiculo().getPesoMaximo();
        for (DTOPrecioFlete flete : fletePrecioResult) {
            Flete listFletesAux = fleteManager.obtenerFletesProximoPosible(flete.getFleteId(), horaFinFlete, horaAComparar, volumen, peso);
            if (listFletesAux != null) {
                listFletes.add(listFletesAux);
            }
        }

        DTODetalleFlete detalle = null;
        List<DTODetalleFlete> resultado = new ArrayList<>();
        if (listFletes.size() > 0) {
            for (int i = 0; i < listFletes.size(); i++) {
                detalle = new DTODetalleFlete(listFletes.get(i).getId(), listFletes.get(i).getCliente().getNombre(), listFletes.get(i).getCliente().getApellido(),
                        listFletes.get(i).getFechaHora(), null, listFletes.get(i).getPrecioSolicitado(), listFletes.get(i).getPeso(), listFletes.get(i).getVolumen(), listFletes.get(i).getEstado(), 0,
                        listFletes.get(i).getPuntoOrigen(), listFletes.get(i).getPuntoDestino());
                resultado.add(detalle);
            }
        }
        return resultado;
    }

    public List<DTOSolicitudFlete> obtenerSolicitudesPendientes(DTOFiltroFechas consulta) {
        fleteroManager = new UsuarioFleteroManager(entityManager);
        fleteManager = new FleteManager(entityManager);

        Fletero fletero = fleteroManager.obtenerUsuario(consulta.getUsuarioId());
        Double peso = fletero.getVehiculo().getPesoMaximo();
        Double volumen = fletero.getVehiculo().getVolumenMaximo();

        List<DTOSolicitudFlete> fletes = new ArrayList<>();
        Calendar cal = Calendar.getInstance();
        cal.setTime(consulta.getFechaHasta());
        cal.add(Calendar.DATE, 1);
        consulta.setFechaHasta(cal.getTime());
        List<Flete> listaFletes = fleteManager.obtenerSolicitudesPendientes(consulta.getFechaDesde(), consulta.getFechaHasta(), volumen, peso);

        for (int i = 0; i < listaFletes.size(); i++) {
            DTOSolicitudFlete solicitud = new DTOSolicitudFlete(listaFletes.get(i).getId(), listaFletes.get(i).getCliente().getId(),
                    listaFletes.get(i).getCliente().getNombre(), listaFletes.get(i).getCliente().getApellido(),
                    listaFletes.get(i).getFechaHora(), listaFletes.get(i).getPeso(),
                    listaFletes.get(i).getVolumen(), listaFletes.get(i).getPrecioSolicitado(),
                    listaFletes.get(i).getPuntoOrigen(), listaFletes.get(i).getPuntoDestino());
            fletes.add(solicitud);
        }
        return fletes;
    }

    public void PuntuarFlete(Long fleteId, Integer puntaje) {
        fleteManager = new FleteManager(entityManager);
        Flete flete = fleteManager.obtenerFletePorId(fleteId);
        flete.setPuntuacion(puntaje);
        fleteManager.actualizarFlete(flete);
    }
    
    public void AceptarFlete(Long fleteId, Long fleteroId, Double precio){
        fleteManager = new FleteManager(entityManager);
        fleteroManager = new UsuarioFleteroManager(entityManager);
        
        Fletero fletero = fleteroManager.obtenerUsuario(fleteroId);
        Flete flete = fleteManager.obtenerFletePorId(fleteId);
        
        flete.setPrecioFinal(Math.round(precio * 100.0) / 100.0);
        flete.setEstado(EstadoFlete.Confirmado);
        flete.setFletero(fletero);
        fleteManager.actualizarFlete(flete);
//        fletero.getFletes().add(flete);
//        
//        fleteroManager.actualizarUsuario(fletero);
    }
}
