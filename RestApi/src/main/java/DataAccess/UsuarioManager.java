
package DataAccess;

import Entities.Usuario;
import javax.persistence.EntityManager;


public abstract class UsuarioManager {
    
    protected EntityManager entityManager;
    
    public void grabarUsuario(Usuario usu){
        entityManager.getTransaction().begin();
        entityManager.persist(usu);
        entityManager.getTransaction().commit();
    };
    
    public void actualizarUsuario(Usuario usu)
    {
        entityManager.getTransaction().begin();
        entityManager.merge(usu);
        entityManager.getTransaction().commit();
    }   
    
    public abstract Usuario obtenerUsuario(Long id);
    
    public abstract Boolean existeEmail(String email);
    
    public abstract Usuario obtenerUsuario(String email);
}
