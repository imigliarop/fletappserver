package DataAccess;

import Entities.ZonaDeTrabajo;
import javax.persistence.EntityManager;

public class ZonaDeTrabajoManager {
     private final EntityManager entityManager;
     
    public ZonaDeTrabajoManager(EntityManager em){
        this.entityManager = em;
    }
    
    public void grabarZonaDeTrabajo(ZonaDeTrabajo zona){
        entityManager.getTransaction().begin();
        entityManager.persist(zona);
        entityManager.getTransaction().commit();
    };
}
