package DataAccess;

import Entities.Flete;
import Enums.EstadoFlete;
import java.util.List;
import java.util.Date;
import javax.persistence.EntityManager;
import javax.persistence.Query;

public class FleteManager {

    private final EntityManager entityManager;

    public FleteManager(EntityManager em) {

        this.entityManager = em;
    }

    ;
    

    public void grabarFlete(Flete flete) {
        entityManager.getTransaction().begin();
        entityManager.persist(flete);
        entityManager.getTransaction().commit();
    }

    ;
    
    public Flete obtenerFletesProximoPosible(String ptoId, Date desde, Date hasta, Double volumen, Double peso) {
        Query q = entityManager.createQuery("SELECT f FROM Flete f WHERE "
                + "f.fechaHora >= :desde and "
                + "f.fechaHora <= :hasta and "
                + "f.estado = 0 and "
                + "f.peso <= :peso and "
                + "f.volumen <= :volumen and "
                + "f.puntoOrigen = :pto");
        q.setParameter("desde", desde);
        q.setParameter("hasta", hasta);
        q.setParameter("pto", ptoId);
        q.setParameter("peso", peso);
        q.setParameter("volumen", volumen);
        List<Flete> response = q.getResultList();
        if (response.isEmpty()) {
            return null;
        } else {
            return response.get(0);
        }

    }

    ;

   
    public void actualizarFlete(Flete flete) {
        entityManager.getTransaction().begin();
        entityManager.merge(flete);
        entityManager.getTransaction().commit();
    }

    public Flete obtenerFletePorId(Long id) {
        return entityManager.find(Flete.class, id);
    }

    public List<Flete> obtenerSolicitudesPendientes(Date desde, Date hasta, Double volumen, Double peso) {

        Query q = entityManager.createQuery(" SELECT f FROM Flete f "
                + " WHERE f.estado = :estado AND "
                + " f.fechaHora >= :desde AND"
                + " f.fechaHora < :hasta AND "
                + " f.peso <= :peso And f.volumen <= :volumen ");
        q.setParameter("estado", EstadoFlete.Solicitado);
        q.setParameter("desde", desde);
        q.setParameter("hasta", hasta);
        q.setParameter("peso", peso);
        q.setParameter("volumen", volumen);
        List<Flete> response = q.getResultList();
        return response;
    }
;
}
