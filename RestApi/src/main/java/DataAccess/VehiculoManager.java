package DataAccess;

import Entities.Vehiculo;
import javax.persistence.EntityManager;

public class VehiculoManager {
    private final EntityManager entityManager;
     
    public VehiculoManager(EntityManager em){
        this.entityManager = em;
    }
    
    public void grabarVehiculo(Vehiculo vehiculo){
        entityManager.getTransaction().begin();
        entityManager.persist(vehiculo);
        entityManager.getTransaction().commit();
    };
    
    public void actualizarVehiculo(Vehiculo vehiculo)
    {
        entityManager.getTransaction().begin();
        entityManager.merge(vehiculo);
        entityManager.getTransaction().commit();
    } 
}
