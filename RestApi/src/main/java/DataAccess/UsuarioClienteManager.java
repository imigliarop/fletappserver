package DataAccess;

import Entities.Cliente;
import Entities.Flete;
import Enums.EstadoFlete;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

public class UsuarioClienteManager extends UsuarioManager {
     
    public UsuarioClienteManager(EntityManager em){
        this.entityManager = em;
    }
    
    @Override
    public Cliente obtenerUsuario(Long id){
        return entityManager.find(Cliente.class, id);
    }
    
    @Override
    public Boolean existeEmail(String email){
        Query q = entityManager.createQuery("SELECT e FROM Cliente c Join c.emails e WHERE e = :email");
        q.setParameter("email", email);
        List<String> response = q.getResultList();
        return !response.isEmpty();
    }
    
    @Override
    public Cliente obtenerUsuario(String email){
        Query q = entityManager.createQuery("SELECT c FROM Cliente c Join c.emails e WHERE e = :email");
        q.setParameter("email", email);
        Cliente f = (Cliente) q.getSingleResult();
        return f;
    }
    
    public List<Flete> listarFletesPendientesCliente(Long id, Date desde, Date hasta) {
        Query q = entityManager.createQuery("SELECT f FROM Flete f Join f.cliente c WHERE c.id = :id and f.estado = :estado and (f.fechaHora >= :desde or :desde IS NULL) and (f.fechaHora <= :hasta or :hasta IS NULL)");
        q.setParameter("id", id);
        q.setParameter("estado", EstadoFlete.Solicitado);
        q.setParameter("desde", desde);
        q.setParameter("hasta", hasta);
        List<Flete> response = q.getResultList();
        return response;
    }
    
    public List<Flete> listarFletesRealizadosCliente(Long id, Date desde, Date hasta) {
        Query q = entityManager.createQuery("SELECT f FROM Flete f Join f.cliente c WHERE c.id = :id and f.estado <> :estado and (f.fechaHora >= :desde or :desde IS NULL) and (f.fechaHora <= :hasta or :hasta IS NULL)");
        q.setParameter("id", id);
        q.setParameter("estado", EstadoFlete.Solicitado);
         q.setParameter("desde", desde);
        q.setParameter("hasta", hasta);
        List<Flete> response = q.getResultList();
        return response;
    }
}
