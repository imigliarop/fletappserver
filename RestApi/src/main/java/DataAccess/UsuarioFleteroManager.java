package DataAccess;

import Entities.Flete;
import Entities.Fletero;
import javax.persistence.EntityManager;
import java.util.List;
import javax.persistence.Query;

public class UsuarioFleteroManager extends UsuarioManager {

    public UsuarioFleteroManager(EntityManager em) {
        this.entityManager = em;
    }

    @Override
    public Fletero obtenerUsuario(Long id) {
        return entityManager.find(Fletero.class, id);
    }

    @Override
    public Boolean existeEmail(String email) {
        Query q = entityManager.createQuery("SELECT e FROM Fletero f JOIN f.emails e WHERE e = :email");
        q.setParameter("email", email);
        List<String> response = q.getResultList();
        return !response.isEmpty();
    }

    @Override
    public Fletero obtenerUsuario(String email) {
        Query q = entityManager.createQuery("SELECT f FROM Fletero f JOIN f.emails e WHERE e = :email");
        q.setParameter("email", email);
        Fletero f = (Fletero) q.getSingleResult();
        return f;
    }

    public List<Fletero> obtenerFleterosParaSugerir(List<String> zonasIds, Double volumen, Double peso) {
        Query q = entityManager.createQuery(" SELECT DISTINCT(f) FROM Fletero f "
                + " JOIN f.zonasDeTrabajo z "
                + " JOIN f.vehiculo v "
                + " WHERE z.id IN :zonas And v.pesoMaximo >= :peso And v.volumenMaximo >= :volumen");
        q.setParameter("zonas", zonasIds);
        q.setParameter("peso", peso);
        q.setParameter("volumen", volumen);
        List<Fletero> response = q.getResultList();
        return response;
    }

    public List<Flete> obtenerProximosFletes(Long fleteroId) {
        Query q = entityManager.createQuery(" SELECT flete FROM Fletero usuario "
                + " Join usuario.fletes flete "
                + " Where usuario.id = :fleteroId And flete.fechaHora > CURRENT_DATE");
        q.setParameter("fleteroId", fleteroId);
        List<Flete> response = q.getResultList();
        return response;
    }

}
