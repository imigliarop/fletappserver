package Enums;


public enum EstadoFlete {
    Solicitado,
    Confirmado,
    Cancelado
}
