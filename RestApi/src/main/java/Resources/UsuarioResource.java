package Resources;

import BusinessLogic.BlUsuario;
import Entities.Vehiculo;
import Entities.ZonaDeTrabajo;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("usuario")
public class UsuarioResource {

    private final BlUsuario blUsuario;
    
    public UsuarioResource(){
        blUsuario = new BlUsuario();
    }
    
    @POST
    @Path("zonaDeTrabajo")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response agregarZonaDeTrabajo(@QueryParam("fleteroId") Long fleteroId, ZonaDeTrabajo zonaDeTrabajo) {
        try {
            List<ZonaDeTrabajo> zonas = blUsuario.AgregarZonaDeTrabajo(fleteroId, zonaDeTrabajo);

            GenericEntity<List<ZonaDeTrabajo>> entity
                    = new GenericEntity<List<ZonaDeTrabajo>>(zonas) {
            };

            return Response.ok(entity).build();
        } catch (Exception ex) {
            //Retorna un 400 si no se pudo crear el usuario
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }
    
    @GET
    @Path("sugerirFleterosParaFlete")
    @Produces(MediaType.APPLICATION_JSON)
    public Response sugerirFleterosParaFlete(@QueryParam("zonasIds") List<String> zonasIds, @QueryParam("volumen") Double volumen,
                                             @QueryParam("peso") Double peso, @QueryParam("distancia") Double distancia){
        
        
        String json = blUsuario.ObtenerFleterosParaSugerir(zonasIds, volumen, peso, distancia);
    
        return Response.ok(json).build();
    }
    
    @GET
    @Path("zonasPorFletero")
    @Produces(MediaType.APPLICATION_JSON)
    public Response obtenerZonasPorIdFletero(@QueryParam("fleteroId") Long fleteroId){
        String json = blUsuario.ObtenerZonasDeFletero(fleteroId);
        
        return Response.ok(json).build();
    }
    
    @GET
    @Path("vehiculoFletero")
    @Produces(MediaType.APPLICATION_JSON)
    public Response obtenerVehiculoFletero(@QueryParam("fleteroId") Long fleteroId){
        Vehiculo v = blUsuario.ObtenerVehiculoDeFletero(fleteroId);
        
        return Response.ok(v).build();
    }
    
    @POST
    @Path("vehiculoFletero")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response setearVehiculoFletero(@QueryParam("fleteroId") Long fleteroId, Vehiculo vehiculo){
        vehiculo = blUsuario.GuardarVehiculo(fleteroId, vehiculo);
        
        return Response.ok(vehiculo).build();
    }
}
