package Resources;

import BusinessLogic.BlFlete;
import DTO.DTODetalleFlete;
import DTO.DTOFiltroFechas;
import DTO.DTOSolicitudFlete;
import DTO.DTOPosibles;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.json.simple.JSONObject;

@Path("flete")
public class FleteResource {

    private final BlFlete blFlete;

    public FleteResource() {
        blFlete = new BlFlete();
    }

    @POST
    @Path("solicitar")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response postJson(DTOSolicitudFlete solicitud) {
        try {
            Long fleteId = blFlete.SolicitarFlete(solicitud);

            //Preparo la respuesta
            Map<String, String> respuesta = new HashMap<>();
            respuesta.put("mensaje", "Su solicitud fue realizada.");
            respuesta.put("idFlete", fleteId.toString());
            return Response.ok(JSONObject.toJSONString(respuesta)).build();
        } catch (Exception ex) {
            //Retorna un 400 Bad Request si no se puede crear el flete
            return Response.ok(Response.Status.BAD_REQUEST).build();
        }
    }

    @GET
    @Path("proximosFletes")
    @Produces(MediaType.APPLICATION_JSON)
    public Response obtenerProximosFletes(@QueryParam("fleteroId") Long fleteroId) {
        List<DTODetalleFlete> fletes = blFlete.ObtenerProximosFletes(fleteroId);

        GenericEntity<List<DTODetalleFlete>> entity
                = new GenericEntity<List<DTODetalleFlete>>(fletes) {
        };

        return Response.ok(entity).build();
    }

    @POST
    @Path("obtenerListaFletesPendientes")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response obtenerListaFletesPendientes(DTOFiltroFechas filtro) {
        List<DTODetalleFlete> fletes = blFlete.obtenerListaFletesPendientesCliente(filtro);

        GenericEntity<List<DTODetalleFlete>> entity
                = new GenericEntity<List<DTODetalleFlete>>(fletes) {
        };

        return Response.ok(entity).build();
    }

    @POST
    @Path("obtenerListaFletesRealizados")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response obtenerListaFletesRealizados(DTOFiltroFechas filtro) {
        List<DTODetalleFlete> fletes = blFlete.obtenerListaFletesRealizadosCliente(filtro);

        GenericEntity<List<DTODetalleFlete>> entity
                = new GenericEntity<List<DTODetalleFlete>>(fletes) {
        };

        return Response.ok(entity).build();
    }

    @POST
    @Path("obtenerSolicitudesPendientes")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response obtenerSolicitudesPendientes(DTOFiltroFechas consulta) {
        List<DTOSolicitudFlete> fletes = blFlete.obtenerSolicitudesPendientes(consulta);

        GenericEntity<List<DTOSolicitudFlete>> entity
                = new GenericEntity<List<DTOSolicitudFlete>>(fletes) {
        };

        return Response.ok(entity).build();
    }

    @POST
    @Path("puntuarFlete")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response puntuarFlete(DTODetalleFlete flete) {
        blFlete.PuntuarFlete(flete.getId(), flete.getPuntuacion());
        return Response.ok().build();
    }

    @GET
    @Path("aceptarFlete")
    @Produces(MediaType.APPLICATION_JSON)
    public Response aceptarFlete(@QueryParam("fleteroId") Long fleteroId, @QueryParam("fleteId") Long fleteId, @QueryParam("precio") Double precio) {
        blFlete.AceptarFlete(fleteId, fleteroId, precio);

        return Response.ok().build();
    }

    @POST
    @Path("sugerirFletePosible")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response sugerirFletePosible(@QueryParam("fleteroId") Long fleteroId, @QueryParam("horaFinFlete") Long horaFinFlete, List<DTOPosibles> fletesPosibles) {
        Date horaFin = new Date(horaFinFlete);
        List<DTODetalleFlete> fletes = blFlete.obtenerFletesProximoPosible(fleteroId, horaFin, fletesPosibles);

        GenericEntity<List<DTODetalleFlete>> entity
                = new GenericEntity<List<DTODetalleFlete>>(fletes) {
        };

        return Response.ok(entity).build();
    }

}
