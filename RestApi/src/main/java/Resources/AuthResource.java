package Resources;

import BusinessLogic.BLAuth;
import BusinessLogic.BlUsuario;
import DTO.DTOUsuario;
import Entities.Usuario;
import Enums.TipoUsuario;
import java.util.HashMap;
import java.util.Map;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.json.simple.JSONObject;

@Path("auth")
public class AuthResource {
    
    private final BLAuth blAuth;
    
    public AuthResource(){
        blAuth = new BLAuth();
    }
    
    @POST
    @Path("login")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response login(@FormParam("email") String email, @FormParam("password") String password, @FormParam("tipo") String tipo) {

        try {
            Usuario usu = blAuth.Autenticar(email, password, TipoUsuario.valueOf(tipo));
            
            JSONObject json = new JSONObject();
            json.put("tipo", usu.getClass().getSimpleName().toLowerCase());
            json.put("id", usu.getId());
            json.put("nombre", usu.getNombre());
            json.put("apellido", usu.getApellido());
            
            return Response.ok(json.toJSONString()).build();
            
        } catch (Exception e) {
            //Retorna 401 Unathorized si el usuario o la password son incorrectos
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }      
    }
    
    @POST
    @Path("registrar")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response registrar(DTOUsuario usuario) {
        try{
            blAuth.CrearUsuario(usuario);
            Map<String, String> respuesta = new HashMap<>();
            respuesta.put("mensaje","Usuario creado correctamente");
            return Response.ok(JSONObject.toJSONString(respuesta)).build();
        }catch(Exception ex){
            //Retorna un 400 si no se pudo crear el usuario
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }
}
