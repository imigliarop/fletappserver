package DTO;

import java.util.Date;

public class DTOSolicitudFlete {
    
    private Long FleteId;
    private Long ClienteId;
    private String Nombre;
    private String Apellido;
    private Date FechaHora;
    private Double Peso;
    private Double Volumen;
    private Double PrecioSolicitado;
    private String PuntoOrigen;
    private String PuntoDestino;
    
    public DTOSolicitudFlete(){
    }
    
    public DTOSolicitudFlete(Long FleteId, Long ClienteId, String Nombre, String Apellido, Date FechaHora, Double Peso, Double Volumen, Double PrecioSolicitado, String PuntoOrigen, String PuntoDestino ){
        this.FleteId = FleteId;
        this.ClienteId = ClienteId;
        this.Nombre = Nombre;
        this.Apellido = Apellido;
        this.FechaHora = FechaHora;
        this.Peso = Peso;
        this.Volumen = Volumen;
        this.PrecioSolicitado = PrecioSolicitado;
        this.PuntoOrigen = PuntoOrigen;
        this.PuntoDestino = PuntoDestino;
    }

    public Long getFleteId() {
        return FleteId;
    }

    public void setFleteId(Long FleteId) {
        this.FleteId = FleteId;
    }
    
    public Long getClienteId() {
        return ClienteId;
    }

    public void setClienteId(Long ClienteId) {
        this.ClienteId = ClienteId;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }
    
    public String getApellido() {
        return Apellido;
    }

    public void setApellido(String Apellido) {
        this.Apellido = Apellido;
    }
    
    public Date getFechaHora() {
        return FechaHora;
    }

    public void setFechaHora(Date FechaHora) {
        this.FechaHora = FechaHora;
    }

    public Double getPeso() {
        return Peso;
    }

    public void setPeso(Double Peso) {
        this.Peso = Peso;
    }

    public Double getVolumen() {
        return Volumen;
    }

    public void setVolumen(Double Volumen) {
        this.Volumen = Volumen;
    }

    public Double getPrecioSolicitado() {
        return PrecioSolicitado;
    }

    public void setPrecioSolicitado(Double PrecioSolicitado) {
        this.PrecioSolicitado = PrecioSolicitado;
    }

    public String getPuntoOrigen() {
        return PuntoOrigen;
    }

    public void setPuntoOrigen(String PuntoOrigen) {
        this.PuntoOrigen = PuntoOrigen;
    }

    public String getPuntoDestino() {
        return PuntoDestino;
    }

    public void setPuntoDestino(String PuntoDestino) {
        this.PuntoDestino = PuntoDestino;
    }
    
    
    
}
