/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author tonga
 */
public class DTOPosibles {
    private Long PtoInicio;
    private Long Zona;

    public Long getPtoInicio() {
        return PtoInicio;
    }

    public void setPtoInicio(Long PtoInicio) {
        this.PtoInicio = PtoInicio;
    }

    public Long getZona() {
        return Zona;
    }

    public void setZona(Long Zona) {
        this.Zona = Zona;
    }
    
}
