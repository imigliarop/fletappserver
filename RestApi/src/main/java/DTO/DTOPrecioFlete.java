/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author tonga
 */
public class DTOPrecioFlete {
    private String fleteId;
    private Double precio;
    public DTOPrecioFlete(){
        
    }
    public DTOPrecioFlete(String id, Double precio){
        this.fleteId = id;
        this.precio = precio;
    }
    public String getFleteId() {
        return fleteId;
    }

    public void setFleteId(String fleteId) {
        this.fleteId = fleteId;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

}
