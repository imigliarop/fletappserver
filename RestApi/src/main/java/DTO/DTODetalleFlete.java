package DTO;

import Enums.EstadoFlete;
import java.util.Date;

public class DTODetalleFlete {

    private Long Id;
    private String Nombre;
    private String Apellido;
    private Date Fecha;
    private Double PrecioFinal;
    private Double PrecioSolicitado;
    private Double Peso;
    private Double Volumen;
    private EstadoFlete Estado;
    private Integer Puntuacion;
    private String PuntoOrigen;
    private String PuntoDestino;

    public DTODetalleFlete() {
    }

    public DTODetalleFlete(Long Id,String Nombre, String Apellido, Date Fecha, Double PrecioFinal, Double PrecioSolicitado, Double Peso, Double Volumen,EstadoFlete Estado, Integer Puntuacion, String PuntoOrigen, String PuntoDestino) {

        this.Id = Id;
        this.Nombre = Nombre;
        this.Apellido = Apellido;
        this.Fecha = Fecha;
        this.PrecioFinal = PrecioFinal;
        this.PrecioSolicitado = PrecioSolicitado;
        this.Peso = Peso;
        this.Volumen = Volumen;
        this.Estado = Estado;
        if (Puntuacion == null) {
            this.Puntuacion = 0;
        } else {
            this.Puntuacion = Puntuacion;
        }
        this.PuntoOrigen = PuntoOrigen;
        this.PuntoDestino = PuntoDestino;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getApellido() {
        return Apellido;
    }

    public void setApellido(String Apellido) {
        this.Apellido = Apellido;
    }

    public Date getFecha() {
        return Fecha;
    }

    public void setFecha(Date Fecha) {
        this.Fecha = Fecha;
    }

    public Double getPrecioFinal() {
        return PrecioFinal;
    }

    public void setPrecioFinal(Double PrecioFinal) {
        this.PrecioFinal = PrecioFinal;
    }

    public Double getPrecioSolicitado(){
        return PrecioSolicitado;
    }
    
    public void setPrecioSolicitado(Double PrecioSolicitado){
        this.PrecioSolicitado = PrecioSolicitado;
    }
    
    public Double getPeso() {
        return Peso;
    }

    public void setPeso(Double Peso) {
        this.Peso = Peso;
    }

    public Double getVolumen() {
        return Volumen;
    }

    public void setVolumen(Double Volumen) {
        this.Volumen = Volumen;
    }

    public EstadoFlete getEstado() {
        return Estado;
    }

    public void setEstado(EstadoFlete Estado) {
        this.Estado = Estado;
    }

    public Integer getPuntuacion() {
        return Puntuacion;
    }

    public void setPuntuacion(Integer Puntuacion) {
        this.Puntuacion = Puntuacion;
    }

    public String getPuntoOrigen() {
        return PuntoOrigen;
    }

    public void setPuntoOrigen(String PuntoOrigen) {
        this.PuntoOrigen = PuntoOrigen;
    }

    public String getPuntoDestino() {
        return PuntoDestino;
    }

    public void setPuntoDestino(String PuntoDestino) {
        this.PuntoDestino = PuntoDestino;
    }

}
