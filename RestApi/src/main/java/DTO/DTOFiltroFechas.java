/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.util.Date;

/**
 *
 * @author tonga
 */
public class DTOFiltroFechas {
    private Long UsuarioId;
    private Date FechaDesde;
    private Date FechaHasta;

    public Long getUsuarioId() {
        return UsuarioId;
    }

    public void setUsuarioId(Long UsuarioId) {
        this.UsuarioId = UsuarioId;
    }

    public Date getFechaDesde() {
        return FechaDesde;
    }

    public void setFechaDesde(Date FechaDesde) {
        this.FechaDesde = FechaDesde;
    }

    public Date getFechaHasta() {
        return FechaHasta;
    }

    public void setFechaHasta(Date FechaHasta) {
        this.FechaHasta = FechaHasta;
    }
    
}
